<?php
/**
 * Page principale (Main page)
 */

// Commencer une session avec un nom et une durée de vie spécifiques
// Start a session with a specific name and lifetime
session_name('WEB' . date('Ymd'));
session_start(['cookie_lifetime' => 3600]);

// Charger automatiquement les classes
// Autoload classes
spl_autoload_register(function ($class) {
    require __DIR__ . '/' . strtolower(str_replace('\\', DIRECTORY_SEPARATOR, $class)) . '.php';
});

// Charger l'autoloader de Composer
// Load Composer's autoloader
require_once __DIR__ . '/vendor/autoload.php';

// Se connecter à la base de données
// Connect to the database
$connect = \app\Helpers\DB::connect();

// Afficher l'entête, le menu, le contenu par défaut et le pied de page
// Display the header, menu, default content, and footer
require_once __DIR__ . '/view/header.html';
require_once __DIR__ . '/view/menu.php';
require_once __DIR__ . '/view/default.php';
require_once __DIR__ . '/view/footer.html';